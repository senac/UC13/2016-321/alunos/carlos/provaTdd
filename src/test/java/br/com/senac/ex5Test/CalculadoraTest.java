package br.com.senac.ex5Test;

import br.com.senac.ex5.Calculadora;
import static br.com.senac.ex5.Numeral.PAR;
import br.com.senac.ex5.Numeros;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculadoraTest {

    @Test
    public void deveDizerSeEParOuImpar() {
        Numeros numero = new Numeros(3);
        Calculadora calculadora = new Calculadora();
        boolean resultado = calculadora.calcular();

        assertEquals(true, resultado);

    }

}
