
package br.com.senac.ex5;

public class Numeros {
    
    private int numero;

    public Numeros() {
    }

    public Numeros(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
    
    
}
